### Run in Idea.

This app sends requests to the ping-pong app, view the [application.ylml](web-server/src/main/resources/application.yml) to see expected ping-pong URL.
So run ping-pong app first. (If ping-pong is not running - nothing scary, you will just see an error message.)

Then run [timestamp generator service](timestamp-generator/src/main/java/org/example/timestampgenerator/TimestampGeneratorApplication.java), 
and the [web-server](web-server/src/main/java/org/example/webserver/WebServerApplication.java).

App is available http://localhost:8091/status

### Run in k8s

Run k3s cluster, forwarding host's ports to ports of agent and load-balancer.
```shell
k3d cluster create -p 8081:80@loadbalancer --agents 2
```

Create a folder in a node required to request a volume
```shell
docker exec k3d-k3s-default-agent-0 mkdir -p /tmp/kube
```

Deploy the application to the cluster.

```shell
kubectl apply -f manifests/k3s/namespace.yml
```
```shell
kubectl apply -f manifests/k3s/volume
```
```shell
kubectl apply -f manifests/k3s
```

App is available http://localhost:8081/status

### Run in Google Kubernetes Engine

Deploy [manifests](manifests/GKE):
namespase, then logoutput config map, then logoutput, and ingress.
