package org.example.webserver.controllers;

import lombok.RequiredArgsConstructor;
import org.example.webserver.services.StatusService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/status")
public class StatusController {

    private final StatusService statusService;

    @GetMapping
    public String getStatus() {
        return statusService.getStatus();
    }
}
