package org.example.webserver.services;

public interface StatusService {
    String getStatus();
}
