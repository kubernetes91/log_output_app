package org.example.webserver.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class PingPongHttpRequester implements PingPongRequester {

    private final HttpRequest pingsRequest;

    private final HttpClient httpClient;

    public PingPongHttpRequester(
            @Value("${ping_pong.uri}") String pingPongUri
    ) throws URISyntaxException {
        pingsRequest = HttpRequest.newBuilder()
                .uri(new URI(pingPongUri))
                .GET()
                .build();
        httpClient = HttpClient.newHttpClient();
    }

    @Override
    public String getPingsCount() throws IOException, InterruptedException {
        HttpResponse<String> response;
        try {
            response = httpClient.send(pingsRequest, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (Exception ex) {
            return "Request url: " + pingsRequest + " Response: " + ex.toString();
        }
    }
}
