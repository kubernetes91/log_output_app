package org.example.webserver.services;

import java.io.IOException;

public interface PingPongRequester {
    String getPingsCount() throws IOException, InterruptedException;
}
