package org.example.webserver.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class StatusServiceImpl implements StatusService {

    private final Path timestampFile;
    private final Path countFile;

    private final PingPongHttpRequester requester;

    @Value("${env.message}")
    private String message;

    public StatusServiceImpl(
            @Value("${timestamp.file}") String timestampFile,
            @Value("${count.file}") String countFile,
            PingPongHttpRequester requester
    ) {
        this.timestampFile = Path.of(timestampFile);
        this.countFile = Path.of(countFile);
        this.requester = requester;
    }

    @Override
    public String getStatus() {
        String content = "";
        try {
            content += message + " <br>";
            content += Files.readString(timestampFile) + " <br>";
            String count = Files.exists(countFile) ? Files.readString(countFile) : "0";

            content += "\n Pings from file: " + count + " <br>";

            content += "\n Pings from ping-pong endpoint: " + requester.getPingsCount() + " <br>";
        } catch (Exception ex) {
                content += " Get an exception: " + ex;
        }
        return content;
    }
}
