package org.example.timestampgenerator.services;

import java.io.IOException;

public interface TimestampWriter {
    void storeCurrentTimestampInFile() throws IOException;
}
