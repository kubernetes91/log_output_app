package org.example.timestampgenerator.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Random;

@Service
public class ScheduledTimestampWriter implements TimestampWriter {

    private final long periodMillis;
    private final File file;
    private final Random generator = new Random();

    public ScheduledTimestampWriter(
            @Value("${timestamp.periodMillis}") long periodMillis,
            @Value("${timestamp.file}") String filePath) throws IOException {
        this.periodMillis = periodMillis;
        this.file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    @Override
    @Scheduled(fixedRate = 5000)
    public void storeCurrentTimestampInFile() {
        ZonedDateTime timestamp = ZonedDateTime.now(ZoneId.of("UTC"));
        try(var writer = new FileWriter(file)) {
            writer.write(timestamp.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String randomString() {
        return generator
                .ints(97, 123)
                .limit(10)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

}
