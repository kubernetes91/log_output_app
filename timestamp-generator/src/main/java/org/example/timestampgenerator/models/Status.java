package org.example.timestampgenerator.models;

import java.time.ZonedDateTime;

public record Status(ZonedDateTime timestamp, String state) {
}
