package org.example.timestampgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimestampGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimestampGeneratorApplication.class, args);
    }

}
